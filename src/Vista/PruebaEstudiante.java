/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;

/**
 *
 * @author madar
 */
public class PruebaEstudiante {
    
    public static void main(String[] args) {
        Estudiante x=new Estudiante(1,"edna"); // Requisito 1
        Estudiante y=new Estudiante(2, "lina");
        //Utilizar requisito 2:
        System.out.println("El nombre del objeto x es:"+x.getNombre());
        //Utilizar requisito 3:
        if(x.equals(y))
            System.out.println("El objeto x es igual al objeto y");
        else
            System.out.println("El objeto x NO es igual al objeto y");
        //Utilizar requisito 4:
        int comparador=((Comparable)x).compareTo(y);
        String msg="Son iguales x e y";
        switch(comparador)
        {
            case 1:{
                msg="X es mayor que Y";
                break;
            }
            case -1:
                msg="X es menor que Y";
                break;
            }
        
        System.out.println(msg);
        
        //Requisito 5:
        System.out.println(x.toString());
        System.out.println(y.toString());
        
        }
       
        
    }

